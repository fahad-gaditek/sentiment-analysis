<?php 
	require("vendor/autoload.php");
	use predictionio\EngineClient;
	$engineClient = new EngineClient('http://localhost:8000');

	function getColor($var)
	{
	    $val = round($var, 2);
	    if ($val >= 2 && $val < 3)
		return '#808080';
	    else if ($val >= 3)
		return '#008000';
	    else if ($val < 2)
		return '#FF0000';
	}
	function getSentiment($val)
	{
	    if ($val >= 2 && $val < 3)
		return 'Neutral';
	    else if ($val >= 3)
		return 'Positive';
	    else if ($val < 2)
		return 'Negative';
	}
?>
