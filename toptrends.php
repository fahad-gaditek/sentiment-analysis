<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
   <HEAD>
      <TITLE>My first HTML document</TITLE>
		<?php 
			require("vendor/autoload.php");
			require 'helper_functions.php';
			require "twitter.local.php";
			include "menu.php";
			use Abraham\TwitterOAuth\TwitterOAuth;
		?>
   </HEAD>
   <BODY>
		<?php 
			$toa = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
			$places = $toa->get('trends/available');
	
			# use World id if selecte location trends not found	
			$woeid = '1';	
			$loc = 'World';
			foreach ($places as $place) { 
				if ($place->name == TREND_LOCATION) {
					# Replace world id with selected location id if location found		
					$woeid = $place->woeid;
					$loc = "$place->name, $place->country";
				}
			}
			$trends2 = $toa->get('trends/place', ["id" => $woeid])[0]->trends;
			$trends = array_slice($trends2, 0, 25)
		?>
			
		<table class="tg">
			<h1><?php echo "Top Trends for location: $loc"; ?></h1>

			<?php foreach ($trends as $x=>$trend) { 
				# Get tweets for the trend
				$search_results = $toa->get('search/tweets', ["q" => $trend->query, "count" => 50]);	
	
				$tweets_sentiments = [];
				# extract tweet text from results
				foreach ($search_results->statuses as $status) { 
					# Calculate sentiment of tweet
					$response = $engineClient->sendQuery(array('s'=>$status->text));
					$sentiment = $response["sentiment"];
					
					# Push the sentiment in an array, so that we can calulate the average
					array_push($tweets_sentiments, $sentiment);
				}
				# Get average of sentiment of tweets
				$avg = array_sum($tweets_sentiments) / count($tweets_sentiments);
			?>
			  <tr>
			    <th class="tg-c57o"><?php echo $x + 1; echo ".$trend->name"; ?></th>
			    <th style="color:<?php echo getColor($avg); ?>" class="tg-c57o"><?php echo $avg; ?></th>
			    <th class="tg-c57o"><?php echo getSentiment($avg); ?></th>
			  </tr>
			<?php } ?>
		</table>

   </BODY>
</HTML>

